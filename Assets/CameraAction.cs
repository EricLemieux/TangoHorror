﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraAction : MonoBehaviour {
    string cameraPictureFilePath;
    int pictureCount;
    GameObject onScreenImage;

    void TakePicture()
    {
        
        pictureCount++;
        cameraPictureFilePath = "PHOTO" + pictureCount + ".png";
        onScreenImage.SetActive(false);
        Application.CaptureScreenshot(cameraPictureFilePath);
        onScreenImage.SetActive(true);
        GameObject.Find("CameraButton").GetComponent<CameraButton>().SetButtonFalse();
        //GameObject.Find("DebugText").GetComponent<Text>().text = "TEST";
        GameObject.Find("LastImage").GetComponent<ImageTest>().SendMessage("ChangePicture", cameraPictureFilePath);
        GameObject.Find("UIController").GetComponent<UIController>().SendMessage("UpdateImagePath",cameraPictureFilePath);
        GameObject.Find("UIContoller").GetComponent<UIController>().SendMessage("UpdateImageIndex", pictureCount);
        
        // GameObject.Find("DebugText").GetComponent<Text>().text = "IS THIS WORKING";
    }

    // Use this for initialization
    void Start () {
        pictureCount = 0;
        cameraPictureFilePath = "PHOTO";
        onScreenImage = GameObject.Find("LastImage");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    
}
