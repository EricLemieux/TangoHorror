﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GhostMovement : MonoBehaviour {

    bool rayHit, pictureTaken;
    Vector3 wayPoint, direction;
    float speed;
    int count, maxCount;

    GameObject debugText;
    GameObject playercam;

    Color color;

    GameObject waypoints;

    const int targetPhotoCount = 3;
    int photoCount; // number of photos the player has taken of the ghost

    void HitByRay() {
        Debug.Log("I was hit by a ray");
        rayHit = true;
        
    }

    void PictureTaken() {
        pictureTaken = true;

    }

    void Wander() {
        //GameObject.Find("DebugText2").GetComponent<Text>().text = "WANDER HIT";
        
        wayPoint = waypoints.GetComponent<WaypointController>().GetPosition(Random.Range(0,7));

        

       
    }

	// Use this for initialization
	void Start () {
        playercam = GameObject.Find("PlayerCamera");
        waypoints = GameObject.Find("WaypointController");
        photoCount = 0;

        rayHit = false;
        pictureTaken = false;
        wayPoint = new Vector3(-4, 0.8f, -4);
        direction = new Vector3(-4, 0.8f, -4);
        maxCount = 200;
        speed = 0.2f;
        count = 0;

        color = GetComponent<Renderer>().material.color;


        // Debug
        debugText = GameObject.Find("DebugText");
    }
	
	// Update is called once per frame
	void Update () {

        direction = Vector3.Normalize(direction - transform.position);

        //GameObject.Find("DebugText2").GetComponent<Text>().text = wayPoint.x + " : " + wayPoint.y + " : " + wayPoint.z;

        //color.a = 1;
        //color.a = Mathf.Lerp(0,1,Mathf.PingPong(Time.time, 1));
        color.a = 0.2f;
        GetComponent<Renderer>().material.color = color;

        //GameObject.Find("DebugText").GetComponent<Text>().text = transform.position.x + " : " + transform.position.y + " : " + transform.position.z;
        
        transform.LookAt(playercam.GetComponent<Transform>().position);
        transform.forward = transform.forward * -1;

        transform.position = Vector3.MoveTowards(transform.position, playercam.GetComponent<Transform>().transform.position, speed*Time.deltaTime);


        if (rayHit)
        {
            color.a = 1;
            

            if (pictureTaken)
            {

                if (speed > 2.0f)
                {
                    speed += 0.3f;
                }
                pictureTaken = false;
                Wander();
                transform.position = wayPoint;
                //AddPhotoCount();
                GameObject.Find("CameraButton").GetComponent<CameraButton>().SetButtonFalse();
                playercam.GetComponent<CameraAction>().SendMessage("TakePicture");
                //GameObject.Find("DebugText").GetComponent<Text>().text = "IS THIS WORKING";
            }


            rayHit = false;

        }
        else {
            speed = 0.3f;
        }

        //GameObject.Find("DebugText").GetComponent<Text>().text = (transform.position - playercam.GetComponent<Transform>().transform.position).magnitude.ToString();
        if ((playercam.GetComponent<Transform>().transform.position - transform.position).magnitude < 0.2f)
        {
            GameObject.Find("UIController").GetComponent<UIController>().SwitchToPause();
            //Wander();
        }
        


        CheckIfDead();
    }


    void AddPhotoCount()
    {
        photoCount++;

        //debugText.GetComponent<Text>().text = "Ghost photo count: " + photoCount;
    }

    void CheckIfDead()
    {
        if (photoCount == targetPhotoCount)
        {
           // debugText.GetComponent<Text>().text = "Ghost has dieded";
            Destroy(GameObject.Find("Ghost"), 0.0f);
        }
    }

    
}
