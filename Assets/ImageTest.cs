﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageTest : MonoBehaviour {



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void ChangePicture(string filepath)
    {
        GameObject.Find("DebugText").GetComponent<Text>().text = "LAST IMAGE";
        string pathToImage = Application.persistentDataPath + "/" + filepath;
       // GameObject.Find("PauseDebugText").GetComponent<Text>().text = pathToImage;
 
        byte[] bytes = System.IO.File.ReadAllBytes(pathToImage);


        Texture2D tex = new Texture2D(2,2);
        tex.LoadImage(bytes);

        GetComponent<Renderer>().material.mainTexture = tex;

    }

}
