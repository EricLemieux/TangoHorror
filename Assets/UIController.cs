﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIController : MonoBehaviour {

    private GameObject mainUI, pauseMenu;

    string[] filepath;
    int filepathIndex;

    void Awake() {
        mainUI = GameObject.Find("MainUI");
        pauseMenu = GameObject.Find("PauseMenu");
    }

	// Use this for initialization
	void Start () {
        mainUI.SetActive(true);
        pauseMenu.SetActive(false);
        filepath = new string[3];
        filepathIndex = 0;
    }

    public void SwitchToPause() {
        mainUI.SetActive(false);
        pauseMenu.SetActive(true);

        GameObject.Find("CatImageOne").GetComponent<UpdatePicture>().SendMessage("ChangePicture", filepath[0]);
        //pauseMenu.GetComponentInChildren<UpdatePicture>().SendMessage("ChangePicture", filepath[0]);
    }

    public void SwitchToMain() {
        mainUI.SetActive(true);
        pauseMenu.SetActive(false);
    }

    void UpdateImagePath(string newfilepath)
    {
        
        filepath[filepathIndex] = newfilepath;
    }
    void UpdateImageIndex(int nextIndex)
    {
        GameObject.Find("DebugText").GetComponent<Text>().text = "WORKING";
        filepathIndex = nextIndex;
    }

}
