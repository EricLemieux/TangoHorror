﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

    Camera c;
    float startFOV, endFOV;
    float time;

	// Use this for initialization
	void Start () {
        c = this.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        lerpToFOV();
	}

    public void setEndFov(float a_infov)
    {
        endFOV = a_infov;
        startFOV = c.fov;
        time = 0.0f;
    }

    void lerpToFOV()
    {
        if (startFOV != endFOV)
        {
            Mathf.Lerp(startFOV, endFOV, time +Time.deltaTime);
        }
    }
    
}
