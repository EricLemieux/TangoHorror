﻿using UnityEngine;
using System.Collections;

public class Handprint : MonoBehaviour {

    GameObject handprint1;
    GameObject handprint2;
    GameObject handprint3;

    bool activate;

    void Artifact3Hit() {
        handprint1.SetActive(true);
        handprint1.GetComponent<AudioSource>().Play();
        handprint2.SetActive(true);
        handprint3.SetActive(true);
    }

    // Use this for initialization
    void Start () {
        handprint1 = GameObject.Find("handprint1");
        handprint2 = GameObject.Find("handprint2");
        handprint3 = GameObject.Find("handprint3");

        handprint1.SetActive(false);
        handprint2.SetActive(false);
        handprint3.SetActive(false);
        activate = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
